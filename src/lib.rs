use std::collections::BTreeMap;
use std::time;

#[derive(Clone)]
pub struct ProfileSectionData
{
    name: String,
    time: time::Duration,
    count: usize
}

impl std::fmt::Debug for ProfileSectionData
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error>
    {
        //write!(f, "{:?}: {:?}", self.name, self.time)
        write!(f, "({:?}, {:?})", self.count, self.time)
    }
}

impl ProfileSectionData {
    pub fn new(name: String, time: time::Duration, count: usize) -> Self {
        Self {
            name: name,
            time: time,
            count: count
        }
    }

    pub fn combine(&self, other: &Self) -> Self {
        if self.name == other.name {
            Self::new(self.name.clone(), self.time + other.time, self.count + other.count)
        } else {
            self.clone()
        }
    }
}

pub struct ProfileData {
    data: BTreeMap<String, ProfileSectionData>
}

impl std::fmt::Debug for ProfileData {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{:?}", self.data)
    }
}

impl ProfileData {
    pub fn new() -> Self {
        Self {
            data: BTreeMap::new()
        }
    }

    pub fn total_time(&self) -> time::Duration {
        self.data.values().fold(time::Duration::from_secs(0), |a, d| a + d.time)
    }

    pub fn combine(&mut self, other: ProfileData) {
        for data in other.data.values() {
            self.add_section(data);
        }
    }

    pub fn add_section(&mut self, data: &ProfileSectionData) {
        let key = data.name.clone();
        match self.data.remove(&key) {
            Some(v) => { self.data.insert(key, v.combine(data)); },
            None => { self.data.insert(key, data.clone()); }
        };
    }

    pub fn profile_section<F: FnOnce()>(&mut self, name: &str, c: F) {
        let time = profile_section(c);
        self.add_section(&ProfileSectionData::new(name.to_string(), time, 1));
    }

    pub fn profile_get_value<F, T>(&mut self, name: &str, c: F) -> T
    where F: FnOnce() -> T
    {
        let (value, time) = profile_get_value(c);
        self.add_section(&ProfileSectionData::new(name.to_string(), time, 1));
        value
    }
}

pub fn profile_section<F: FnOnce()>(c: F) -> time::Duration
{
    let start = time::Instant::now();
    c();
    time::Instant::now() - start
}

pub fn profile_get_value<F, T>(c: F) -> (T, time::Duration)
where F: FnOnce() -> T
{
    let start = time::Instant::now();
    let result = c();
    (result, time::Instant::now() - start)
}
